-- Inserts para la tabla "integrantes"
INSERT INTO integrantes (matricula, nombre, apellido, codigo, orden, activo) VALUES
('Y25366','Christian','Salinas', '01', 1,1),
('Y18624','Lujan', 'Caceres', '02', 2,1),
('Y12887','Steven', 'Lopez', '03', 3,1),
('3850665','Marcio', 'Saldivar', '04', 4,1);

-- Inserts para la tabla "tipoMedia"
INSERT INTO tipoMedia (id,nombre, orden,activo) VALUES
(1,'YouTube', 1,1),
(2,'Imagen', 2,1),
(3,'Dibujo', 3,1);

-- Inserts para la tabla "media"
INSERT INTO media (id, matricula, titulo, video, scr_propia, scr,  id_tmedia, orden, activo) VALUES

(1,'Y25366', 'Página de Christian Salinas', 'https://www.youtube.com/embed/ZDs_f_ZdluU', '', '',  1, 1,1),
(2,'Y25366', 'Imagen que me identifica', '', '/images/imgpropia.png', '/images/imgpropia.png',  2, 2,1),
(3,'Y25366', 'Imagen que me identifica', '', '', '/images/imgrepresentativa.jpg',  3, 3,1),

(4, 'Y18624', 'Página de Lujan Caceres', 'https://www.youtube.com/embed/Ci5raxp37QE', '', '',  1, 4,1),
(5, 'Y18624', 'Imagen que me identifica', '', '/images/paint_lujan.png', '',  2, 5,1),
(6, 'Y18624', 'Imagen que me identifica', '', '', '/images/isla.jpeg',  3, 6,1),

(7, 'Y12887','Página de Steven Lopez', 'https://www.youtube.com/embed/0aQPX_Iqu-A?si=yrpnMk0IUVFkQtif', '', '',  1, 7,1),
(8, 'Y12887','Imagen que me identifica', '', '/images/20240410_192335.jpg', '',  2, 8,1),
(9, 'Y12887','Imagen que me identifica', '', '', '/images/mejores-anime-largos.webp',  3, 9,1),

(10, '3850665', 'Página de Marcio Saldivar', 'https://www.youtube.com/embed/0aQPX_Iqu-A?si=yrpnMk0IUVFkQtif', '', '',  1, 10,1),
(11, '3850665', 'Imagen que me identifica', '', '/images/isla.jpeg', '',  2, 11,1),
(12, '3850665', 'Imagen que me identifica', '', '', '/images/mejores-anime-largos.webp',  3, 12,1);