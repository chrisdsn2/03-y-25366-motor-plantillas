const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(
    "./db/integrantes.sqlite3",// con el db/ busca desde la carpeta raiz
    sqlite3.OPEN_READWRITE,//que abra el archivo lectura escritura y si no pasa al error
    (error)=>{
        if(error) console.log("Ocurrio un error", error.message);
        else{
            console.log("Conexion Exitosa");
           // db.run("select * from integrantes")// Para verificar que estamos en la bd correcta
        }
    }
);

async function getAll(query, params) {
    return new Promise((resolve, reject) => {
        db.all(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

module.exports = {db,getAll};