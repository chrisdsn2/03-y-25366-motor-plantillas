// archivo que va a contener todas las rutaas publicas del proyecto
const express = require("express");
const router = express.Router();
const {getAll} = require("../db/conexion");
require("dotenv").config();
// Las Rutas de mi proyecto
//......................Direcciones.............
router.use((req,res,next)=>{
    res.locals.ENLACE=process.env.ENLACE;
    res.locals.NOMBRE=process.env.NOMBRE;
    res.locals.APELLIDO=process.env.APELLIDO;    
    res.locals.MATERIA=process.env.MATERIA;
    next();
});

//ruta localhost:{puerto}
router.get("/", async(request,response) => {
    const rows = await getAll("select * from integrantes");
    //console.log(rows);
    response.render("index",{
        integrantes: rows,        
    });
});

//cuando escriba la direccion http://localhost:3000/paginas/wordcloud/index.html
router.get("/wordcloud", async(request,response) => {
    const rows = await getAll("select * from integrantes")
    response.render("wordcloud",{
        integrantes: rows,
    })
});

//cuando escriba la direccion http://localhost:3000/paginas/info_curso/index.html
router.get("/info_curso", async(request,response) => {
    const rows = await getAll("select * from integrantes")
    response.render("info_curso",{
        integrantes: rows,
    })
});

// *****************************************************************
router.get("/:matricula", async (request, response, next) => {
    const matriculas = (await getAll("select matricula from integrantes where activo = 1 order by orden")).map(obj => obj.matricula);
    const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
    const matricula = request.params.matricula;
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");

    if (matriculas.includes(matricula)) {
        const integranteFilter = await getAll("select * from integrantes where activo = 1 and matricula = ? order by orden", [matricula]);
        const mediaFilter = await getAll("select * from media where activo = 1 and matricula = ? order by orden", [matricula]);
        
        //console.log("media", db.media)
        response.render('integrantes', {
            integrante: integranteFilter,
            tipoMedia: tipoMedia,
            media: mediaFilter,
            integrantes: integrantes
        });
    } else {
        next();
    }
});

//exportamos al router
module.exports = router;